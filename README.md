# PostgreSQL interface for PicoLisp

This library implements interfacing to PostgreSQL DBMS. Please read [EXPLAIN](EXPLAIN.md) to learn more about this library implementation.

1. [Requirements](#requirements)
2. [Getting Started](#getting-started)
3. [Usage](#usage)
4. [Examples](#examples)
5. [Testing](#testing)
6. [Contributing](#contributing)
7. [License](#license)

# Requirements

* PicoLisp 64-bit v3.1.11+ (Tested up to PicoLisp v18.9.5)
* libpq (Tested for libpq 11.1 but libpq compatible with previous versions)

# Getting Started

1. Include `pg.l` in your project either by copying pg.l into your project or add it as git submodule
2. Check [examples](#examples) below

# Usage

## Function Reference
* `(pg-start arg1)` establishes connection to PostgreSQL instance.
  - `arg1` is connection string (check out [documentation](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-PARAMKEYWORDS))
* `(pg-finish)` terminates current connection.
* `(pg-execute arg1 . params)` executes query given in `arg1` with given query parameters passed in `params`. Returns list of resulting set where each row represented as list.
* `(pg-trans . prg)` executes `prg` body in transaction. If at the end of `prg` body transaction still not commited and not reverted, it executes `(pg-commit)` at the end of transaction if transaction is without errors. Otherwise, `(pg-rollback)` executed.
* `(pg-commit)` commits current transaction.
* `(pg-rollback)` reverts current transaction.
* `(pg-last-err)` returns error of last executed query or NIL if last query was successful. Its recommended to check this function after each query in transaction. Executed automatically at the end of transaction to check transaction status.
* `(pg-cursor arg1 fun . params)` executes query given in `arg1` with given query parameters passed in `params`. Its lazily retrieves rows of resulting set from database and execute `fun` over each row.
* `(pg-escape-str arg1)` returns escaped version of string passed in `arg1`. Its recommended to escape all strings passed as query parameters to prevent security compromise.

## Exceptions Reference

Exceptions can be thrown only from `(pg-start)` or `(pg-execute)` functions. `(pg-start)` can throw only `'pg-connector-err`.

`(pg-execute)` exceptions:

* `'pg-connector-err` thrown only if its nor possible to connect with backend to client or something wrong with client itself.
* `'pg-data-err` thrown in case of error during data processing at query execution. For example, it is devision by zero.
* `'pg-qexec-err` thrown when problem occured on backend side and can not be handled from client side.
* `'pg-integr-violat-err` thrown when violated integrity of database (i.e. foreign key violation)
* `'pg-prog-err` occurs when passed query with incorrect syntax, incorrect names of database artifacts, wrong number of parameters etc.
* `'pg-trans-err` thrown when occured error that enforces ROLLBACK inside transaction (i.e. deadlock).
* `'pg-db-err` used as fallback for all errors that does not listed in exceptions above.

# Examples

## Basic workflow for executing queries

```picolisp
(load "pg.l")

(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")
(pg-execute "SELECT i, 'text_' || i FROM generate_series(1, 3) AS i;")
# -> ((1 "text_1") (2 "text_2") (3 "text_3"))
...
(pg-finish)
```

## Passing params

```picolisp
(load "pg.l")
(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")

# pass int
(pg-execute "SELECT i, 'text_' || i FROM generate_series(1, $1) i;" 4)
# -> ((1 "text_1") (2 "text_2") (3 "text_3") (4 "text_4"))

# pass str
(pg-execute "SELECT i, $2 || i FROM generate_series(1, $1) i;" 4 "text")
# -> ((1 "text1") (2 "text2") (3 "text3") (4 "text4"))

# pass list of ints
(pg-execute "SELECT $1[i] FROM generate_series(1, 3) i;" '(1 2 3))
# -> ((1) (2) (3))

# pass list of strings
(pg-execute "SELECT length(s) FROM unnest($1) s;" '("a" "ab" "abc"))
# -> ((1) (2) (3))

# pass list of unknown for PicoLisp type but representable in string format
(pg-execute "SELECT f * 2.0 FROM unnest($1::float[]) f;" '("0.1" "0.2" "0.3"))
# -> (("0.2") ("0.4") ("0.6"))
# note that non-primitive type is returned as text
# so keep in mind to treat such values correctly in application
(pg-execute "SELECT i::text::int FROM jsonb_array_elements($1::jsonb) i;" "[1, 2, 3]")
# -> ((1) (2) (3))
...
(pg-finish)
```

## Transactions

```picolisp
(load "pg.l")
(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")
(pg-execute "CREATE TABLE test_table(id int, txt test);")
[pg-trans
   (pg-execute "TRUNCATE TABLE test_table;")
   (pg-execute "INSERT INTO test_table (id, txt) VALUES ($1, $2)" 123 "text")
   (pg-rollback)]
(pg-execute "SELECT * FROM test_table;")
# -> NIL

# autocommit
[pg-trans
   (pg-execute "TRUNCATE TABLE test_table;")
   (pg-execute "INSERT INTO test_table (id, txt) VALUES ($1, $2)" 123 "text")]
(pg-execute "SELECT * FROM test_table")
# -> ((123 "text"))

# autorollback
[pg-trans
   (pg-execute "TRUNCATE TABLE test_table;")
   (catch T (pg-execute "INSERT INTO test_table (id, txt) VALUES ($1, $2)" 123))]
(pg-last-err)
# -> "ERROR: bind message supplies 1 parameters, but prepared statement \"\" requires 2"
(pg-execute "SELECT * FROM test_table")
# -> NIL
...
(pg-finish)
```

## Cursor result set processing

For example our resulting set contains millions of rows so persisting of them in memory is inefficient. We can iterate whole set by batches and perform some kind of processing on each of them individually.
```picolisp
(load "pg.l")
(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")
(let I 0
   (make
      (pg-cursor "SELECT i FROM generate_series(1, $1) i;"
         '((R)
            (inc 'I)
            (link (car R))
            (when (== I 10)
               (my-send-fun (made))
               (made NIL)
               (zero I) ) )
         1000000 ) ) )
...
(pg-finish)
```

## Error checking

```picolisp
(load "pg.l")
(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")
(catch 'pg-data-err (pg-execute "SELECT '1'::timestamp;"))
# -> NIL
# Also verbose error will be printed to stdout
(pg-last-err)
# -> "ERROR: cannot cast type jsonb to integer..."
...
(pg-finish)
```

## Custom types codec

Also `pgint.l` able to set global behaviour for decoding values via `pg-register-codec`. It takes OID of type and decoder `fun` which will perform value conversion. `fun` is actually a S-expr which has access to `V` symbol which stores value of given type. So its enough to return new value based on processing of V.
```picolisp
(load "pg.l")
(pg-start "host=localhost port=5432 dbname=foo user=bar password=baz")
(pg-execute "SELECT '2019-01-01'::date;")
# -> (("2019-01-01"))
# 'date' type is unknown for pgint.l decoder, but we can actually register one
(pg-register-codec
   1082 # OID for 'date' type
   '(date ($dat V "-")) # in decoder will be injected this line, V arg is accessible from decoder.
   )
(pg-execute "SELECT '2019-01-01'::date;")
# -> (((2019 1 1)))
...
(pg-finish)
```

# Testing

Library comes with unit tests ([link](https://github.com/aw/picolisp-unit)). To run tests, exec:
```bash
make check
```

# Contributing

Currently this library in state of development so if you want to help to improve it, please feel free either to create an issue or open MR.

# License

[MIT License](LICENSE.md)

# Maintainer

Nail Gibaev, <abel.ze.normand@gmail.com>