# for tests execution run as 'postgres' user:
# CREATE DATABASE test;
# CREATE TABLE test_table_pk (id serial, PRIMARY KEY (id));
# CREATE TABLE test_table_fk (test_table_pk_id integer REFERENCES test_table_pk(id));
# CREATE TABLE test_table (id integer, txt text);

(load "pg.l")
(load "tests/unit.l")

(de public-funs . '(pg-execute pg-cursor pg-trans pg-commit pg-rollback pg-escape-str pg-start pg-finish pg-register-codec))
(de cleanup-test-tbl () (pg-execute "TRUNCATE test_table;"))
(de insert-test-row-correct () (pg-execute "INSERT INTO test_table (id, txt) VALUES ($1, $2)" 1 "test_val"))
(de insert-test-row-incorrect () (pg-execute "INSERT INTO test_table (id, txt) VALUES ($1, $3)" 1 "test_val"))
(de collect-test-tbl () (pg-execute "SELECT * FROM test_table"))

(pg-start "host=localhost port=5432 dbname=test user=postgres password=")

(cleanup-test-tbl)

[execute
   '(assert-nil
       (pg-execute "SELECT * FROM test_table")
       "empty result should be NIL" )

   '(assert-kind-of 'Number
       (caar (pg-execute "SELECT 1::int"))
       "integer field type should be represented as int picolisp" )

   '(assert-kind-of 'String
       (caar (pg-execute "SELECT 'test_str'::text"))
       "text field type should be represented as transient symbol in picolisp" )

   '(assert-equal "2000-01-01 00:00:00"
       (caar (pg-execute "SELECT '01-01-2000'::timestamp"))
       "nonprimitive type should be represented as transient symbol" )

   '(assert-equal 1
       (caar (pg-execute "SELECT $1::int" 1))
       "when int passed as arg then its encoded correctly and echoed back with int type" )

   '(assert-equal "0.5"
       (caar (pg-execute "SELECT $1::float / 2" 1))
       "when int '1' passed as arg and divided by 2 then result float type should be represented also as transient symbol" )

   '(assert-equal "abc"
       (caar (pg-execute "SELECT $1" "abc"))
       "when transient symbol passed as arg then its interpreted as text and echoed back as transient symbol" )

   '(assert-equal '("ab" 2)
       (car (pg-execute "SELECT $2::text, $1::int" 2 "ab"))
       "when multiple args passed then they are echoed back with correct types" )

   '(assert-equal '((1) (2) (3))
       (pg-execute "SELECT i::int FROM unnest($1::int[]) i" '(1 2 3))
       "when list of ints passed as arg to query and unnested then it should return as one-column set" )

   '(assert-equal '("a" "b" "c")
       (caar (pg-execute "SELECT $1::text[]" '("a" "b" "c")))
       "when list of transient symbols passed as arg to query then it should echo back" )

   '(assert-equal '(1 2 3)
       (caar (pg-execute "SELECT $1::int[]" '(1 2 3)))
       "when list of ints passed as arg to query it should echo back" )

   '(assert-nil
       (prog
          (pg-trans
             (cleanup-test-tbl)
             (insert-test-row-correct)
             (pg-rollback) )
          (let R (collect-test-tbl)
             (cleanup-test-tbl)
             R ) )
       "when row inserted in transaction and rolled back then select for same table should return nil" )

   '(assert-equal '((1 "test_val"))
       (prog
          (pg-trans
             (cleanup-test-tbl)
             (insert-test-row-correct)
             (pg-commit) )
          (let R (collect-test-tbl)
             (cleanup-test-tbl)
             R ) )
       "when row inserted in transaction and commited then select for same table should contain this row" )

   '(assert-equal '((1 "test_val"))
       (prog
          (pg-trans
             (cleanup-test-tbl)
             (insert-test-row-correct) )
          (let R (collect-test-tbl)
             (cleanup-test-tbl)
             R ) )
       "when transaction operations does not explicitly commited or rolled back then in case if there no errors in transaction it will be autocommited" )

   '(assert-t
       (prog
          (pg-trans
             (catch 'pg-db-err (insert-test-row-incorrect)) )
          (let (E (pg-last-err)
                R (collect-test-tbl) )
             (and E (== R NIL)) ) )
       "when transaction in failed state and not explicitly rolled back then transaction reverts automatically" )

   '(assert-t
       (prog
          (pg-trans
             '(catch 'pg-prog-err (insert-test-row-incorrect))
             (when (pg-last-err)
                (pg-rollback) ) )
          (let (E (pg-last-err)
                R (collect-test-tbl) )
             (and (== E NIL) (== R NIL)) ) )
       "when transaction in failed state and explicitly rolled back then (pg-last-err) would return nothing" )

   '(assert-t
       (prog
          (pg-trans
             (cleanup-test-tbl)
             (insert-test-row-correct)
             (unless (pg-last-err)
                (pg-commit) ) )
          (let (E (pg-last-err)
                R (collect-test-tbl) )
             (cleanup-test-tbl)
             (and (== E NIL) (= (car R) '(1 "test_val"))) ) )
       "when transaction explicitly commited then autocommit does not executed" )

   '(assert-equal '(1 1 1)
       (prog
          (cleanup-test-tbl)
          (do 3
             (insert-test-row-correct) )
          (let Res (make
                      (pg-cursor "SELECT * FROM test_table"
                         '((R) (link (car R))) ) )
             (cleanup-test-tbl)
             Res ) )
       "when cursor supplied with callback then it will be executed for each row in returned set" )

   # need to check for over 500 rows in resulting set execution bcs in (pg-cursor) insides rows are retrieved in batches
   # with < 500 rows in each
   '(assert-equal 521
       (prog
          (cleanup-test-tbl)
          (do 521
             (insert-test-row-correct))
          (use Acc
             (zero Acc)
             (pg-cursor "SELECT * FROM test_table" '((R) (inc 'Acc)))
             (cleanup-test-tbl)
             Acc ) )
       "when over 500 rows are in resulting set then all of them will be executed though they all are in different batches" )

   '(assert-nil
       (trim (mapcar lint public-functions))
       "public functions should be linted without errors" )

   '(assert-kind-of 'String
       (pg-escape-str "#abc#ABC\\//”")
       "when unescaped string passed to pg-escape-str then it will be escaped and returned as transient symbol" )

   '(assert-throws 'pg-connector-err NIL
       '(pg-start "port=1")
       "when connection can not be established then interface should throw 'pg-connector-err error" )

   '(assert-throws 'pg-data-err NIL
       '(pg-execute "SELECT 1 / 0;")
       "when executed query which causes error during processing data then 'pg-data-err should be thrown" )

   # pg-qexec-err is impossible to test without direct manipulation with pg backend

   '(assert-throws 'pg-integr-violat-err NIL
       '(pg-execute "INSERT INTO test_table_fk (test_table_pk_id) VALUES (1);")
       "when executed query which causes integrity error in table constraints then 'pg-integ-violat-err should be thrown" )

   '(assert-throws 'pg-prog-err NIL
       '(pg-execute "TABLE;")
       "when executed query with syntax error then 'pg-prog-err should be thrown" )

   ## '(assert-t
   ##     (catch T
   ##        (pg-trans
   ##           (catch 'pg-data-err (pg-execute "SELECT 1/0;"))
   ##           (pg-execute "SELECT 1;") ) ) )

   '(assert-throws 'pg-db-err NIL
       '(pg-trans
           (catch 'pg-data-err (pg-execute "SELECT 1/0;"))
           '(pg-execute "SELECT 1;") )
       "when executed query in failed transaction without rolling back then 'pg-db-err should be thrown" )

   '(assert-equal '(2019 1 1)
       (prog
          (pg-register-codec 1082 '(date ($dat V "-") ) )
          (caar (pg-execute "SELECT '2019-01-01'::date;")) )
       "when registered new codec for nondefault pg datatype then successive queries with result sets containing new data type will be decoded")]

(pg-finish)
