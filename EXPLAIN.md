# Explanation: PostgreSQL interface for PicoLisp

This documents provides some insights about implementation principles and walkthrough for `pgint.l` source code

Make sure you read [README](README.md) in order to understand library public API specifics.

# Why use "procedural"-orietiered API?

Because its more easier to understand and declarative. Also PicoLisp never was and never meant to be pure functional. I thought that it would be more convinient to use dynamic binding feature to simplify API and function declarations. So, in a nutshell, backbone of whole library is wrapping low-level libpq functions into more convinient way.

# Types translation

PostgreSQL by default supports >30 types of values and its even without arrays and other complex types. Its will be redundant to implement translation for all types from PicoLisp to PostgreSQL and backwards because PostgreSQL supports representation of almost all values in plain text. So, its enough to provide translation of basic core types of PicoLisp (integers, strings and lists) so all other types can be derived or parsed from plain text format.

## Value casting by metadata

`libpq` provides API to ask result struct for type of each field in resulting set. Types in PostgreSQL represented as OIDs (Object Identifiers) which are unsigned integers. `cast-foid` function casts value to given type but if its unsupported by PicoLisp, it will be left intact in plain text format.
```picolisp
(de cast-foid (ResH IFld V FOid)
   (case FOid
      (`INTOID (format V))
      (`STROID (use @S
                  (match '("'" @S "'") (chop V))
                  (or (pack @S) V) ) )
      (`INTARROID (use @IL
                     (match '("{" @IL "}") (chop V))
                     (mapcar format (mapcar pack (split @IL ","))) ) )
      (`STRARROID (use @SL
                     (match '("{" @SL "}") (chop V))
                     (mapcar
                        '((S) (use @S
                                 (match '("'" @S "'") S)
                                 (or (pack @S) (pack S)) ) )
                        (split @SL ",") ) ) )
      (T V) ) )
```

Types collection performed in function `format-tuples`.
```picolisp
...
(let FOids (make
              (for IFld NFlds
                 (link (exec-libpq "PQftype" 'I ResH (- IFld 1))) ) ) )
```

## Custom type translation codec

Though that existing type decoding from PostgreSQL to PicoLisp is sufficient, sometimes it is very handy to register global behaviour for all instances of values for given datatype. Function `pg-register-codec` globally defines rule for value parsing to PicoLisp-preferred value.
Check [example in tests](tests/tests.l). Though that date can be represented as text but if we want to perform some manipulation over data, its recommended to declare rule for further value decoding. Keep in mind that each call of `pg-register-codec` actually rewrites existing function responsible for type castings, so your codec can not be "un-registered". I think its OK solution but working with OIDs could be difficult for some users so probably in future `pgint.l` will have special function to query OID for datatype.

# Parameterized queries

Parameterized query can be performed by invocation of `PQexecParams` function. This function supports two options: plain text format and binary representation. At the moment, implemented only encoding in text format, probably it has a little overhead but I did not check. Memory management for array of parameters represented as strings was performed with `pg-alloc-strings` and `pg-free-strings`. Thanks to [this thread](https://www.mail-archive.com/picolisp@software-lab.de/msg07748.html) where Alexander Burger shows some nontrivial solutions to work with native calls.

# Global variables

This library uses 2 primary global variables. `*PGDB` contains pointer to `PGconn` struct and used as first argument in all native calls to `libpq`, `*PGErr` contains last query execution result error. Direct access to this variables does not recommended so better use `pg-start`, `pg-finish` and `pg-last-err`.

# License

This work licensed under MIT License

Copyright (c) 2019 Nail Gibaev, <abel.ze.normand@gmail.com>