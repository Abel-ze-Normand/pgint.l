# pg.l --- PostgreSQL interfacing library
#
# The MIT License (MIT)
#
# Copyright (c) 2019 Nail Gibaev

(setq
   INTOID '(23 20 21)
   STROID 25
   INTARROID 1007
   STRARROID 1009
   PG_DIAG_SQLSTATE (char "C") )

(setq *PGCurCnt 500)

# memory operations functions for (native)

# most of this functions are implemented based on thread:
# https://www.mail-archive.com/picolisp@software-lab.de/msg07748.html
(de pg-alloc-strings (L)
   (mapcar
      '((S) (cons (native "@" "strdup" 'N S) 8))
      L ) )

(de pg-wrap-native-strings (L)
   (let Len (length L)
      (cons NIL (list (* 8 Len)) L) ) )

(de pg-free-strings (L)
   (mapcar
      '((P) (native "@" "free" NIL (car P)))
      L ) )

(de pg-uints-arr (L)
   (cons NIL (make
                (link (cons (* 4 (length L))))
                (for V L
                   (if (=0 V) (link 0 0 0 0) (link (- V))) ) ) ) )

(de pg-ints-arr (L)
   (cons NIL (make
                (link (cons (* 4 (length L))))
                (for V L (link (cons V 4))) ) ) )

# PG interfacing

(de pg-log-err (Msg . @)
   (prinl (pass text (pack "[pgint.l ERROR] " Msg))) )

(de pg-log-warn (Msg . @)
   (prinl (pass (pack "[pgint.l WARN]" Msg))) )

(de exec-libpq (Cmd RetTyp . @)
   (pass native "libpq.so" Cmd RetTyp) )

(de pg-escape-str (Str)
   (exec-libpq "PQescapeLiteral" 'S *PGDB Str (native "@" "strlen" 'I Str)) )

(de pg-start (ConnStr)
   # ConnStr is standard conn string (check out https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-PARAMKEYWORDS)
   # usually its "host=... hostaddr=... port=... dbname=... user=... password=..."
   (let H (exec-libpq "PQconnectdb" 'N ConnStr)
      (unless (=0 (exec-libpq "PQstatus" 'I H))
         (pg-log-err "Error in connecting to database with passed conn string @1" ConnStr)
         (throw 'pg-connector-err) )
      (setq *PGDB H) ) )

(de pg-finish ()
   (exec-libpq "PQfinish" NIL *PGDB) )

(de err-statcode (ResH)
   (exec-libpq "PQresultErrorField" 'S ResH `PG_DIAG_SQLSTATE) )

(de throw-err (ResH)
   (throw
      (let E (chop (err-statcode ResH))
         (pg-get-res-err ResH)
         (case (car E)
            ("2" (case (cadr E)
                    (("0" "1") 'pg-prog-err)
                    ("2" 'pg-data-err)
                    ("3" 'pg-integr-violat-err)
                    (("6" "7" "8") 'pg-exec-err)
                    (T 'pg-db-err) ) )
            ("3" (case (cadr E)
                    (("D" "F") 'pg-prog-err)
                    (T 'pg-db-err) ) )
            ("4" (case (cadr E)
                    ("0" 'pg-trans-err)
                    (T 'pg-prog-err) ) )
            ("5" 'pg-qexec-err)
            (T 'pg-db-err) ) ) ) )

(de cast-foid (V FOid)
   # by default libpq returns data in text format, but from columns metadata we can query field type (OID)
   # all types can be found: https://github.com/postgres/postgres/blob/master/src/include/catalog/pg_type.h
   (case FOid
      (`INTOID (format V))
      (`STROID (use @S
                  (match '("'" @S "'") (chop V))
                  (or (pack @S) V) ) )
      (`INTARROID (use @IL
                     (match '("{" @IL "}") (chop V))
                     (mapcar format (mapcar pack (split @IL ","))) ) )
      (`STRARROID (use @SL
                     (match '("{" @SL "}") (chop V))
                     (mapcar
                        '((S) (use @S
                                 (match '("'" @S "'") S)
                                 (or (pack @S) (pack S)) ) )
                        (split @SL ",") ) ) )
      (T V) ) )

(de tup_val+cast (ResH ITup IFld FOid)
   (cast-foid (exec-libpq "PQgetvalue" 'S ResH ITup IFld) FOid))

(de format-tuples (ResH)
   (let (NFlds (exec-libpq "PQnfields" 'I ResH)
         NTups (exec-libpq "PQntuples" 'I ResH)
         FOids (make
                  (for IFld NFlds
                     (link (exec-libpq "PQftype" 'I ResH (- IFld 1))) ) ) )
      (make
         (for I NTups
            (link
               (make
                  (let _FOids FOids
                     (for J NFlds
                        (link (tup_val+cast ResH (- I 1) (- J 1) (car _FOids)) )
                        (setq _FOids (cdr FOids)) ) ) ) ) ) ) ) )

(de deduce-arr-typ (Arg)
   (cond
      ((lst? Arg) (deduce-arr-typ (car Arg)))
      ((num? Arg) INTARROID)
      (T STRARROID) ) )

(de pg-encode (Arg)
   # number, text, array, nested array
   (cond
      ((num? Arg) (cons (car INTOID) (sym Arg)))
      ((str? Arg) (cons STROID Arg))
      ((lst? Arg) (cons (deduce-arr-typ Arg) (pack "{" (glue "," (mapcar cadr (mapcar pg-encode Arg))) "}")))
      ((sym? Arg) (cons STROID (sym Arg)))
      (T (throw 'pg-connector-err (text "Unknown type of val for encoding: @1" Arg))) ) )

(de pg-last-err () *PGErr)

(de pg-get-res-err (ResH) (setq *PGErr (exec-libpq "PQresultErrorMessage" 'S ResH)))

(de pg-chk-res (ResH)
   (wipe '*PGErr)
   (exec-libpq "PQresultStatus" 'I ResH) )

(de pg-get-results (ResH)
   (case (pg-chk-res ResH)
      ((0 1) NIL)
      (2
         (let Tups (format-tuples ResH)
            (exec-libpq "PQclear" NIL ResH)
            Tups ) )
      (7 (nil
            (pg-log-err "Postgres fatal error: @1" (pg-get-res-err ResH))
            (throw-err ResH)) )
      (T (nil
            (pg-log-warn "Unknown query execution result status met: @1" @) ) ) ) )

(de pg-execute (Q . @)
   # executes query greedily returns everything immediately
   # pass query string in Q and in rest of params would be passed to query execution
   (let (EncArgs (mapcar pg-encode (rest))
         ArgC (length EncArgs)
         Oids (pg-uints-arr (mapcar car EncArgs))  # i.e. types
         AllocArgs (pg-alloc-strings (mapcar cadr EncArgs))
         ResH (exec-libpq "PQexecParams" 'N *PGDB Q ArgC Oids (pg-wrap-native-strings AllocArgs) NIL (pg-ints-arr (need ArgC 0)) 0) )
      (pg-free-strings AllocArgs)
      (pg-get-results ResH) ) )

(de pg-begin ()
   (setq *PGTrans T)
   (pg-execute "BEGIN") )

(de pg-commit ()
   (when *PGTrans
      (pg-execute "COMMIT")
      (wipe '*PGTrans) ) )

(de pg-rollback ()
   (when *PGTrans
      (pg-execute "ROLLBACK")
      (wipe '*PGTrans) ) )

(de pg-trans X
   # autocommits at the end of transaction if transaction does not end with pg-commit or pg-rollback
   (pg-begin)
   (mapc eval X)
   (let ErrBckUp (pg-last-err)
      (if ErrBckUp
         (pg-rollback)
         (pg-commit) )
      # restore last error bcs pg-rollback in that case would erase *PGErr
      (setq *PGErr ErrBckUp) ) )

(de pg-cursor (Q Fun . @)
   (let PortNam (text "portal_@1" (hash Q))
      (pg-trans
         (pass pg-execute (text "DECLARE @1 CURSOR FOR @2" PortNam Q))
         (use B
            (loop
               (setq B (pg-execute (text "FETCH FORWARD @1 IN @2" *PGCurCnt PortNam)))
               (mapcar Fun B)
               (NIL B NIL) ) )
         (pg-execute (text "CLOSE @1" PortNam)) ) ) )

(de pg-register-codec (Oid Prg)
   (let (CasBody (cadr cast-foid)
         Codec (list Oid Prg)
         NewCasBody (insert (length CasBody) CasBody Codec) )
      (eval (list 'redef 'cast-foid (car cast-foid) NewCasBody)) ) )